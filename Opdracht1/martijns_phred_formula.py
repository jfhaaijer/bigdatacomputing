#! /usr/bin/env python3
"""
This program calculates the average phred score per base position
from a fastqfile. It writes these scores to csv file. Certain arguments
can be given in the command line:
Inputfile = A fastq file
-o output = The name of the file to write the results to. Contains the
base positions and their average quality score.
-n number of threads = the number of threads to use. Defaults to the
number of online cpu's available.
-b bases = Change the basepositions printed to the screen when no
output file is mentioned. Default is 100, thus each 100th baseposition
and their average quality score is printed to the screen.

Author: Jildou Haaijer
Date: 7-05-2020
"""

import re
import math
import csv
import sys
from multiprocessing import Process, Queue
import multiprocessing
import argparse

class FastqReader():
    """
    This class has methods to process a fastq file and
    extract the average quality score per position.
    """

    def __init__(self, filename, ascii_base=33):
        self.filename = filename
        self.nested_score_list = []
        self.ascii_base = ascii_base
        self.avg_score_per_base = []

    def get_file_length(self):
        """
        Function to get to total number of rows from a file and the
        length of the longest line. A list variable is create with the same
        ammount of lists as the longest line.
        """
        num_lines = sum(1 for line in open(self.filename))
        max_len = len((max(open(self.filename), key=len))) - 1
        self.nested_score_list = [[]for _ in range(max_len)]
        return num_lines, self.nested_score_list

    def read_fastq(self, lines, task, chunksize, return_dict):
        """
        This method reads each line of the file and creates a list of
        the quality score indicatiors per position. This step is divided by
        multiple processes. Each process reads a number of lines and creates
        a list. These lists are later combined into one.
        """
        for line in lines[task : task+chunksize]:
            # Skip all the lines  exept the quality score line
            if re.match("[ACTGU@+]", line[0]):
                continue

            for char, position in zip(line, range(len(line)-1)):
                if char == "\n": # Skip the end of the line
                    continue

                # Add the character to the position list
                self.nested_score_list[position].append(char)

        return_dict[task] = [self.nested_score_list]

    def add_scores_to_list(self, n_score_list):
        """
        Calculate the phred scores and combine it into a list.
        Input: List with lists of quality indicatiors for each position
        Output: List with average phred score per position
        """
        # Calculate the average phred score per base position list
        print("    Calculating average score per position...")
        for score_list in n_score_list:
            self.avg_score_per_base.append(self.calculate_phred_score(score_list))
        print("    Scores per position are calculated. \n")
        return self.avg_score_per_base

    def calculate_phred_score(self, position_list):
        """
        Calculate average basecall quality of a read.
        Receive the integer quality scores of a read and return the average quality for that read
        First convert Phred scores to probabilities,
        calculate average error probability
        convert average back to Phred scale
        """

        # total = sum([10**((ord(q) - self.ascii_base) / -10) for q in position_list])
        # score = -10 * math.log(total / len(position_list), 10)
        # return round(score, 3)

        total = sum([ord(q) for q in position_list])

        return round(total/len(position_list), 2)

def write_csv(scorelist, outputfile):
    """
    Write the scores per position to a csvfile.
    Column 1: Base_position : int
    Column 2: Average_Base_quality : int
    """
    with open(outputfile, mode='w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"')
        # Write header
        csv_writer.writerow(["Base_Position", "Average_Base_quality"])

        # Write the average base quality per base position to the csv file
        for pos, avg_q in zip(range(len(scorelist)), scorelist):
            csv_writer.writerow([pos+1, avg_q])
        csv_file.close()

        print("File is written. Results can be found in {}.".format(outputfile))


def main(args):
    """
    Main function: Executes the program steps.
    Gets arguments from the command line with argparse. Uses
    multiprocessing increase processing speed.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("inputfile", help="Fastq format file")
    parser.add_argument("-n", '--threads', dest="number_of_threads",
                        default=multiprocessing.cpu_count(),
                        help="Number of threads to use. (Default=total number of online cpus)",
                        type=int)
    parser.add_argument('-o', '--output',
                        help="Name of the outputfile in csv format.")
    parser.add_argument("-b", "--bases", default=10000, type=int,
                        help="Select the n-th base positions to print to the screen when no \
                            outputfile is given. (Default=10000)")
    parser.add_argument('--ascii', dest="ascii_base", default=33, type=int,
                        help="Ascii base value. Default = 33. Can be either 33 or 64.")

    args = parser.parse_args()

    print("Information:")
    print('    Input file: {}'.format(args.inputfile))
    print('    Output file: {}'.format(args.output))
    print("    Available online cpu's: {}".format(multiprocessing.cpu_count()))
    print('    Number of threads used: {} \n'.format(args.number_of_threads))

    manager = multiprocessing.Manager()
    return_dict = manager.dict()

    if args.ascii_base != 33:
        args.ascii_base = 64

    filereader = FastqReader(filename=args.inputfile, ascii_base=args.ascii_base)
    rows, longlist = filereader.get_file_length()
    if args.number_of_threads == 1:
        chunk_size = rows
    else:
        chunk_size = round(rows/args.number_of_threads)
    chunk_queue = Queue()

    with open(args.inputfile, "r") as fastqfile:
        lines = fastqfile.readlines()

        for number in range(rows):
            if number % chunk_size == 0:
                chunk_queue.put(number)

        print("Start processing the file:")
        for process in range(args.number_of_threads):
            if chunk_queue.empty():
                break
            name = "Process " + str(process)
            task = chunk_queue.get()
            proc = Process(target=filereader.read_fastq,
                           name=name,
                           args=(lines, task, chunk_size, return_dict))
            proc.start()
            print("    {} is working on lines {} : {}".format(proc.name, task, task+chunk_size))
            proc.join()
        print("    File is processed.")

    # Combine the resulting lists of each process into one list
    for process in return_dict:
        for score_list in  return_dict[process]: # Loop through each list withing the list.
            # (each list has the values for that position of the lines from 1 process)
            for position_list, pos in zip(score_list, range(len(score_list))):
                if position_list == []: # Check if the position lists are not empty
                    continue
                for item in position_list:
                    longlist[pos].append(item) # Add the values for each position to a final list
                                               # This list has lists for each position

    # Calculate scores
    scores = filereader.add_scores_to_list(longlist)

    # Check if the outputfile was given
    if args.output is not None:
        # Write to csv file
        write_csv(scores, args.output)
    else:
        print("Results:")
        for position, score in zip(range(len(scores)), scores):
            if position % args.bases == 0:  # Print a part of the output to the screen
                if position != 0:
                    print("    Base position: {}    Average quality score: {} \
                        ".format(position, score))

if __name__ == "__main__":
    main(sys.argv[1:])
