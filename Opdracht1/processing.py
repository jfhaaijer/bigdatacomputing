from multiprocessing import Lock, Process, Queue, current_process
import time
import queue
import os
import multiprocessing
import argparse


def child1():
        print(current_process().name)
def child2():
        print(current_process().name)

class reader():
    def __init__(self):
        self.listoflines = []
        self.chunks = []

    def read_file(self, lines, task, chunksize, number=0):
        linelist = []
        i = 0
        number +=1
            # task = queue.get()
        print("task nr:", task)
        for line in lines[task: task+chunk_size]:
                #print(i, line)
            linelist.append(i)
            i += 1
        print(linelist)
            #self.listoflines.append(linelist)
           # print(self.listoflines)
        self.listoflines.append(linelist)
        self.chunks.append(number)
            #print(self.listoflines)
    
    def connect_lists(self):
        l = [[],[],[]]
        #process_list = outlist
        for n, linelist in zip(self.chunks, self.listoflines):
            l[n-1].append(linelist)
        print(l)
        return l
        

    #def output(self):
        #print(self.listoflines)



if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Fastq format file")
    parser.add_argument('-o', '--output', default="fastq_avg_scores.csv",
                        help="Name of the outputfile in csv format. Default is fastq_avg_scores.csv")
    parser.add_argument('-n', dest='number_of_threads', default=multiprocessing.cpu_count(),
                        help="Number of threads to use. Default is all.")
    args = parser.parse_args()
    # ... do something with args.output ...
    # ... do something with args.verbose ..

    filename = "test.fastq"
    nlines = 28
    processes = multiprocessing.cpu_count()
    queue = Queue()
    chunk_size = 10
    llist = []

    # a = [[1,2], [3,4], [5,6]]
    # b = [[9,8], [7,6], [5,4]]
    # c = []
    # for ai in range(len(a)):
    #     c[i].append(a[i])
    #     c[i].append(b[i])
    # for ai, bi in zip(a,b):
    #     x = ai + bi
    #     c.append(x)
    # print(c)

    list1 = [[], [], [], [], [], 'text', 'text2', [], 'moreText']
    list2 = [e for e in list1 if e]
    print("list2:", list2)
    f = reader()
    for i in range(nlines):
        if i % chunk_size == 0:
            #print(i)
            queue.put(i)

    
    with open(filename, "r") as file:
        lines = file.readlines()
        while not queue.empty():
            for process in range(processes):
                if queue.empty():
                    break
                name = "Process: " + str(process)
                task = queue.get()
                print(task)
                proc = Process(target=f.read_file, name=name, args=(lines, task, chunk_size))
                print(proc.name , "is working")
                proc.start()
                proc.join()
                
                #read_file(file, task, task+chunk_size)
        #f.output()

    l = f.connect_lists()
    print(l)

