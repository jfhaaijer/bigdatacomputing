#! /usr/bin/env python3
"""
This program calculates the average phred score per base position
from a fastqfile. It writes these scores to csv file. Certain arguments
can be given in the command line:
Inputfile = A fastq file
-o output = The name of the file to write the results to. Contains the
base positions and their average quality score.
-n number of threads = the number of threads to use. Defaults to the
number of online cpu's available.
-b bases = Change the basepositions printed to the screen when no
output file is mentioned. Default is 100, thus each 100th baseposition
and their average quality score is printed to the screen.

Author: Jildou Haaijer
Date: 7-05-2020
"""

import multiprocessing as mp
from multiprocessing.managers import BaseManager, SyncManager
from multiprocessing import Queue
import os, sys, time, queue, csv, re, math
from fastqreader import * 
import argparse
from datetime import datetime

POISONPILL = "MEMENTOMORI"
ERROR = "DOH"
IP = 'localhost'
PORTNUM = 8805
AUTHKEY = b'whathasitgotinitspocketsesss?'
data = ["Always", "look", "on", "the", "bright", "side", "of", "life!"]


def make_server_manager(port, authkey):
    """ Create a manager for the server, listening on the given port.
        Return a manager object with get_job_q and get_result_q methods.
    """
    job_q = queue.Queue()
    result_q = queue.Queue()

    # This is based on the examples in the official docs of multiprocessing.
    # get_{job|result}_q return synchronized proxies for the actual Queue
    # objects.
    class QueueManager(BaseManager):
        pass

    QueueManager.register('get_job_q', callable=lambda: job_q)
    QueueManager.register('get_result_q', callable=lambda: result_q)

    manager = QueueManager(address=('', port), authkey=authkey)
    manager.start()
    print('Server started at port %s' % port)
    return manager

def runserver(fn, chunk_list, chunk_size):
    # Start a shared manager server and access its queues
    manager = make_server_manager(PORTNUM, b'whathasitgotinitspocketsesss?')
    shared_job_q = manager.get_job_q()
    shared_result_q = manager.get_result_q()
    
    if not chunk_list:
        print("Gimme something to do here!")
        return
    
    print("Sending data!")
    for chunk in chunk_list:
        shared_job_q.put({'fn' : fn, 'chunk' : chunk, 'chunk_size': chunk_size})
    
        time.sleep(2)  
    
    results = []
    while True:
        try:
            result = shared_result_q.get_nowait()
            results.append(result)
            print("Got result!", result)
            if len(results) == len(chunk_list):
                print("Got all results!")
                break
        except queue.Empty:
            time.sleep(1)
            continue

    # Tell the client process no more data will be forthcoming
    print("Time to kill some peons!")
    shared_job_q.put(POISONPILL)
    # Sleep a bit before shutting down the server - to give clients time to
    # realize the job queue is empty and exit in an orderly way.
    time.sleep(5)
    print("Aaaaaand we're done for the server!")
    manager.shutdown()
    print(results)

def make_client_manager(ip, port, authkey):
    """ Create a manager for a client. This manager connects to a server on the
        given address and exposes the get_job_q and get_result_q methods for
        accessing the shared queues from the server.
        Return a manager object.
    """
    class ServerQueueManager(BaseManager):
        pass

    ServerQueueManager.register('get_job_q')
    ServerQueueManager.register('get_result_q')

    manager = ServerQueueManager(address=(ip, port), authkey=authkey)
    manager.connect()

    print('Client connected to %s:%s' % (ip, port))
    return manager

def runclient(num_processes):
    manager = make_client_manager(IP, PORTNUM, AUTHKEY)
    job_q = manager.get_job_q()
    result_q = manager.get_result_q()
    run_workers(job_q, result_q, num_processes)
    
def run_workers(job_q, result_q, num_processes):
    processes = []
    for p in range(num_processes):
        temP = mp.Process(target=peon, args=(job_q, result_q))
        processes.append(temP)
        temP.start()
    print("Started %s workers!" % len(processes))
    for temP in processes:
        temP.join()

def peon(job_q, result_q):
    my_name = mp.current_process().name
    while True:
        try:
            job = job_q.get_nowait()
            if job == POISONPILL:
                job_q.put(POISONPILL)
                print("Aaaaaaargh", my_name)
                return
            else:
                try:
                    result = job['fn'](job['chunk'], job['chunk_size'])
                    print("Peon %s Workwork on %s!" % (my_name, job['chunk']))
                    result_q.put({'job': job, 'result' : result})
                except NameError:
                    print("Can't find yer fun Bob!")
                    result_q.put({'job': job, 'result' : ERROR})

        except queue.Empty:
            print("sleepytime for", my_name)
            time.sleep(1)


def main(args):
    """
    Main function: Executes the program steps.
    Gets arguments from the command line with argparse. Uses
    multiprocessing increase processing speed.
    """
    
    parser = argparse.ArgumentParser()
    parser.add_argument("inputfile", help="Fastq format file")
    # parser.add_argument("inputfile", help="Fastq format file")
    parser.add_argument("-n", '--threads', dest="number_of_threads",
                        default=mp.cpu_count(),
                        help="Number of threads to use. (Default=total number of online cpus)",
                        type=int)
    parser.add_argument('-o', '--output',
                        help="Name of the outputfile in csv format.")
    parser.add_argument("-b", "--bases", default=10000, type=int,
                        help="Select the n-th base positions to print to the screen when no \
                            outputfile is given. (Default=10000)")
    parser.add_argument('--ascii', dest="ascii_base", default=33, type=int,
                        help="Ascii base value. Default = 33. Can be either 33 or 64.")

    args = parser.parse_args()



    print("Information:")
    print('    Input file: {}'.format(args.inputfile))
    print('    Output file: {}'.format(args.output))
    print("    Available online cpu's: {}".format(mp.cpu_count()))
    print('    Number of threads used: {} \n'.format(args.number_of_threads))


    if args.ascii_base != 33:
        args.ascii_base = 64

    filereader = FastqReader(filename=args.inputfile, outputfile= args.output, threads=4,ascii_base=args.ascii_base)
    rows, longlist = filereader.get_file_length()

    chunk_list = []

    if args.number_of_threads == 1:
        chunk_size = rows
    else:
        chunk_size = round(rows/args.number_of_threads)

    for number in range(rows):
        if number % chunk_size == 0:
            chunk_list.append(number)

    # if args.server:
    server = mp.Process(target=runserver, args=(filereader.read_fastq, chunk_list, chunk_size))
    server.start()
    time.sleep(1)
    # if args.client:
    client = mp.Process(target=runclient, args=(4,))
    client.start()
    client.join()
    server.join()

    # cli = mp.Process(target=filereader.combine_results(r))
    # cli.start()
    # cli.join()
    # server.join()


if __name__ == "__main__":
    start=datetime.now()
    main(sys.argv[1:])
    print("\n Runtime: {}\n".format(datetime.now()-start))
