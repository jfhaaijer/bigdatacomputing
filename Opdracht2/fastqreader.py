import csv, re, math, statistics

class FastqReader():
    """
    This class has methods to process a fastq file and
    extract the average quality score per position.
    """

    def __init__(self, filename, outputfile, threads=1, ascii_base=33):
        self.filename = filename
        self.nested_score_list = []
        self.ascii_base = ascii_base
        self.avg_score_per_base = []
        self.outputfile = outputfile
        with open(filename, "r") as fastqfile:
            lines = fastqfile.readlines()
        self.lines = lines
        self.result_per_chunk = []


    def get_file_length(self):
        """
        Function to get to total number of rows from a file and the
        length of the longest line. A list variable is create with the same
        ammount of lists as the longest line.
        """
        self.num_lines = sum(1 for line in open(self.filename))
        max_len = len((max(open(self.filename), key=len))) - 1
        self.nested_score_list = [[]for _ in range(max_len)]
        return self.num_lines, self.nested_score_list

    def read_fastq(self, task, chunk_size):
        """
        This method reads each line of the file and creates a list of
        the quality score indicatiors per position. This step is divided by
        multiple processes. Each process reads a number of lines and creates
        a list. These lists are later combined into one.
        """  
        longest_line = len((max(self.lines[task : task+chunk_size], key=len))) - 1
        nested_score_list = [[]for _ in range(longest_line)]

        for line in self.lines[task : task+chunk_size]:
            # Skip all the lines  exept the quality score line
            if re.match("[ACTGU@+]", line[0]):
                continue

            for char, position in zip(line, range(len(line)-1)):
                if char == "\n": # Skip the end of the line
                    continue

                # Add the character to the position list
                self.nested_score_list[position].append(char)

        # avg_scores = self.add_scores_to_list(nested_score_list)
        # self.result_per_chunk.append(avg_scores)
        # for item, pos in zip(avg_scores, range(len(avg_scores))):
        #     self.nested_score_list[pos].append(item)


        if task == len(self.lines) - chunk_size:
            results = self.add_scores_to_list(self.nested_score_list)
            self.write_csv(results)
    
        # # return the average scores per pos for this chunk
        return len(self.nested_score_list)
    
    # def combine_results(self, result_queue):
    #     while not result_queue.empty: 
    #         job_result = result_queue.get()
    #         for item, pos in zip(job_result, range(len(job_result))):
    #             self.nested_score_list[pos].append(item)
    #     results = self.calculate_mean_per_chunk(self.nested_score_list)
    #     self.write_csv(results)

    # def calculate_mean_per_chunk(self, nested_list):
    #     results = []
    #     for position_list in nested_list:
    #         if len(position_list) > 1:
    #             results.append(sum(position_list)/ len(position_list))
    #         else:
    #             results.append(sum(position_list))
    #     return results

    def add_scores_to_list(self, n_score_list):
        """
        Calculate the phred scores and combine it into a list.
        Input: List with lists of quality indicatiors for each position
        Output: List with average phred score per position
        """
        # Calculate the average phred score per base position list
        print("    Calculating average score per position...")
        avg_score_per_base = []
        for score_list in n_score_list:
            # self.avg_score_per_base.append(self.calculate_phred_score(score_list))
            avg_score_per_base.append(self.calculate_phred_score(score_list))
        print("    Scores per position are calculated. \n")
        return avg_score_per_base


    def calculate_phred_score(self, position_list):
        """
        Calculate average basecall quality of a read.
        Receive the integer quality scores of a read and return the average quality for that read
        First convert Phred scores to probabilities,
        calculate average error probability
        convert average back to Phred scale
        """
        total = sum([ord(q) for q in position_list])
        if total == 0:
            return 0
        else:
            return round(total/len(position_list), 2)

    def write_csv(self, scorelist):
        """
        Write the scores per position to a csvfile.
        Column 1: Base_position : int
        Column 2: Average_Base_quality : int
        Example:
            pos : score
            1 : 20
            2 : 30 
            3 : 5
        """
        with open(self.outputfile, mode='w') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"')
            # Write header
            csv_writer.writerow(["Base_Position", "Average_Base_quality"])

            # Write the average base quality per base position to the csv file
            for pos, avg_q in zip(range(len(scorelist)), scorelist):
                csv_writer.writerow([pos+1, avg_q])
            csv_file.close()

            print("File is written. Results can be found in {}.".format(self.outputfile))
